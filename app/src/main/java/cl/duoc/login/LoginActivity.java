package cl.duoc.login;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cl.duoc.login.bd.Almacen;
import cl.duoc.login.entidades.Formulario;

public class LoginActivity extends AppCompatActivity  {

    private Button btnIngresar , btnLimpiar,btnReg;
    private EditText edContra, edUsuario;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnIngresar =(Button) findViewById(R.id.btnIngresar);
        btnLimpiar = (Button)findViewById(R.id.btnLimpiar);
        btnReg=(Button)findViewById(R.id.btnReg);
        edContra=(EditText) findViewById(R.id.edContra);
        edUsuario=(EditText)findViewById(R.id.edUsuario);

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             ingresar();
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
        btnReg.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this , FormularioActivity.class);
                startActivity(i);
            }
        });


    }

    private void limpiar() {
        edUsuario.setText("");
        edContra.setText("");

    }

    private void ingresar() {

        boolean  b = false;

        for (Formulario f: Almacen.formularios )
        {
            if(edUsuario.getText().length()>0 &&edUsuario.getText().length()>0){
            if(f.getUsuario().equals(edUsuario.getText().toString())&& f.getClave().equals(edContra.getText().toString()))
            {
                b=true;
            }}

        }

        if(b)
        {
            Toast.makeText(this, " Ingresado", Toast.LENGTH_SHORT).show();
        }

    }







}

