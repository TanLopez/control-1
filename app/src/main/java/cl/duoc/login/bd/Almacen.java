package cl.duoc.login.bd;

import java.util.ArrayList;

import cl.duoc.login.entidades.Formulario;


/**
 * Created by DUOC on 25-03-2017.
 */

public class Almacen {
  public static ArrayList<Formulario> formularios = new ArrayList<>();

    public static void agregarFormulario(Formulario formulario){

        formularios.add(formulario);
    }

    public static ArrayList<Formulario> getFormularios(){
        return formularios;
    }
}
