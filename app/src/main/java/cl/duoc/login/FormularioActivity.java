package cl.duoc.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cl.duoc.login.bd.Almacen;
import cl.duoc.login.entidades.Formulario;

public class FormularioActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText edUsuario, edNombres, edApellidos,edRut, edFecha, edClave, edClave2;
    private Button btnRegistrar, btnVolver , btnLimpiar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);

        edUsuario= (EditText) findViewById(R.id.edUsuario);
        edNombres= (EditText) findViewById(R.id.edNombres);
        edApellidos= (EditText) findViewById(R.id.edApellidos);
        edRut= (EditText) findViewById(R.id.edRut);
        edFecha= (EditText) findViewById(R.id.edFecha);
        edClave= (EditText) findViewById(R.id.edClave);
        edClave2= (EditText) findViewById(R.id.edClave2);

        btnLimpiar= (Button)findViewById(R.id.btnLimpiar);
        btnLimpiar.setOnClickListener(this);

        btnRegistrar= (Button)findViewById(R.id.btnRegistrar);
        btnRegistrar.setOnClickListener(this);

        btnVolver= (Button)findViewById(R.id.btnVolver);
        btnVolver.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        if(v.getId()== R.id.btnLimpiar)
        {
            edUsuario.setText("");
            edNombres.setText("");
            edApellidos.setText("");
            edRut.setText("");
            edFecha.setText("");
            edClave.setText("");
            edClave2.setText("");

        }
        else if(v.getId()== R.id.btnRegistrar)
        {
            String mensajeError = "";

            if(edUsuario.getText().toString().length() < 1){
                mensajeError += "Ingrese usuario \n";
            }
            if(edNombres.getText().toString().length() < 1){
                mensajeError += "Ingrese Nombres \n";
            }
            if(edApellidos.getText().toString().length()<1){
                mensajeError += "Ingrese Apellidos \n";
            }
            if(!isValidarRut(edRut.getText().toString())){
                mensajeError += "Ingrese un rut valido \n";
            }

            if(!edClave.getText().toString().equals(edClave2.getText().toString()) ){
                mensajeError += "Verifique que las claves sean iguales \n";
            }

            if(mensajeError.length()>0){
                Toast.makeText(this, mensajeError, Toast.LENGTH_SHORT).show();
            }else{

                Formulario nuevoF = new Formulario();
                nuevoF.setUsuario(edUsuario.getText().toString());
                nuevoF.setNombres(edNombres.getText().toString());
                nuevoF.setApellidos(edApellidos.getText().toString());
                nuevoF.setRut(edRut.getText().toString());
                nuevoF.setClave(edClave.getText().toString());

                Almacen.agregarFormulario(nuevoF);
                Toast.makeText(this, "Guardado correctamente", Toast.LENGTH_LONG).show();
            }





        }
        else if (v.getId()== R.id.btnVolver)
        {
            Intent i = new Intent(FormularioActivity.this , LoginActivity.class);
            startActivity(i);

        }





    }

    private boolean isValidarRut(String rut) {

        boolean validacion = false;
        try {
            rut =  rut.toUpperCase();
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

            char dv = rut.charAt(rut.length() - 1);

            int m = 0, s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }
            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }

        } catch (java.lang.NumberFormatException e) {
        } catch (Exception e) {
        }
        return validacion;
    }
}
